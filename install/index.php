<?php
    include '../protected/vendors/phpBB/sql_parse.php';
    session_start();
    $APP                = 'Yii Blog Bootstrap';
    $VERSION            = '0.1';
    $DB_SCHEMA          = '../protected/data/schema.mysql.sql';
    $DB_DATA            = '../protected/data/data.mysql.sql';
    $DB_TABLE_PREFIX    = 'tbl_';
    // {{{ Requirements and Tests
	$requirements = array(
		array(
			'PHP 5.1.0+',
			version_compare(PHP_VERSION,"5.1.0",">="),
			'important',
			'Your version of PHP is less than 5.1.0. Please upgrade your PHP installation'
		),
		array(
			'Reflection Class Installed',
			class_exists('Reflection',false),
			'important',
			'Could not find Reflection'
		),
		array(
			'PCRE Extension Installed',
			extension_loaded("pcre"),
			'important',
			'Could not find PCRE Extension. Please check your PHP installation.'
		),
		array(
			'SPL Extension Install',
			extension_loaded("SPL"),
			'error',
			'Could not find SPL Extension'
		),
		array(
			'DOM Extension Installed',
			class_exists("DOMDocument",false),
			'important',
			'Could not find DOM extension'
		),
		array(
			'PDO Extension Installed',
			extension_loaded('pdo'),
			'important',
			'Could not find PDO Extension'
		),
		array(
			'PDO MySQL Extension Installed',
			extension_loaded('pdo_mysql'),
			'important',
			'Could not find PDO MySQL Extension'
		),
		array(
			'Memcache Installed',
			(extension_loaded("memcache") || extension_loaded("memcached")),
			'warning',
			'Your server currently does not support Memcache. To improve your performance, install Memcache'
		),
		array(
			'APC Cache Installed',
			extension_loaded("apc"),
			'warning',
			'Your server currently does not support APC Cache. To improve your performance, install APC Cache'
		),
		array(
			'Assets Directory is Writable',
			is_writable('../assets'),
			'important',
			$APP.' cannot write to the ciims/assets directory'
		),
		array(
			'Runtime Directory is Writable',
			is_writable('../protected/runtime'),
			'important',
			$APP.' cannot write to the ciims/protected/runtime directory'
		),
		array(
			'Config Directory is Writable',
			is_writable('../protected/config'),
			'important',
			$APP.' cannot write to the ciims/protected/config directory'
		),
		array(
		    'index.php is writable',
		    is_writable('../index.php'),
		    'important',
		    $APP.' cannot update the boostrapper'
		)
	); // }}} 
	
	// Ajax handlers
	if (isset($_POST) && !empty($_POST))
	{
		// {{{ Yii Path Checking
		if (isset($_POST['yiiCheck']))
		{
		    $path = $_POST['yiiCheck']['path'];
		    $r = substr($path, -1);
		    // if ($r !== '/' or $r!==' )
		    //    $path .= DIRECTORY_SEPARATOR;
			if (file_exists($path.'yiilite.php')) 
			{
				$_SESSION['CiiInstaller']['yiiPath'] = $path;
			}
			else
				header('ERROR', false, 406);
		} // }}}  
		else if (isset($_POST['systemCheck'])) // {{{
		{
			$errors = false;
			foreach ($requirements as $k=>$v)
			{
				echo '<li>' . $v[0];
				echo '<span class="label label-' . ($v[1] ? 'info' : ($v[2] ? $v[2] : 'important')) .'" ' . (!$v[1] ? 'rel="tooltip" title="' . $v[3]. '"' : ''). '>' . ($v[1] ? 'OK' : (isset($v[2]) ? $v[2] : 'Error')) .'</span>';
				echo '</li>';
				
				if (!$v[1] && !$errors)
				{
					if ($v[2] == 'warning')
						$errors = false;
					else
						$errors = true;
				}
				
				if ($errors)
					header('ERROR', false, 406);
				else
					header('OK', false, 200);
			}
		} // }}} 
		else if (isset($_POST['mysqlCheck'])) // {{{
        {
            try {
                if($_POST['mysqlCheck']['host']=='' or $_POST['mysqlCheck']['user']=='')   die(header('ERROR', false, 406));

                // $conn = mysql_connect($_POST['mysqlCheck']['host'], $_POST['mysqlCheck']['user'], $_POST['mysqlCheck']['password']) or die(header('ERROR', false, 406));
                $dbh = new PDO('mysql:host='.$_POST['mysqlCheck']['host'].';dbname='.$_POST['mysqlCheck']['db'], $_POST['mysqlCheck']['user'], $_POST['mysqlCheck']['password']);

                // $db = mysql_select_db($_POST['mysqlCheck']['db']) or die(header('ERROR', false, 406));
                
                $_SESSION['CiiInstaller']['host'] = $_POST['mysqlCheck']['host'];
                $_SESSION['CiiInstaller']['db'] = $_POST['mysqlCheck']['db'];
                $_SESSION['CiiInstaller']['db_user'] = $_POST['mysqlCheck']['user'];
                $_SESSION['CiiInstaller']['db_pass'] = $_POST['mysqlCheck']['password'];
                $_SESSION['CiiInstaller']['table_prefix'] = $_POST['mysqlCheck']['table_prefix'];

                // Create DB tables
                $sqlDump = @fread(@fopen($DB_SCHEMA, 'r'), @filesize($DB_SCHEMA)) or die(header('ERROR',false,406));
                $sqlDump = remove_remarks($sqlDump);
                // DEBUG par($sqlDump);
                $statements = split_sql_file($sqlDump, ';');
                // DEBUG par($statements);
                // Do all statements
                $sqls = array();
                foreach($statements as $n=>$statement) {
                    if(strtoupper(substr(trim($statement),0,6))=='SELECT')  {
                        $result = $dbh->query($statement);
                    } else {
                        $statement = str_replace(
                            array('CREATE TABLE tbl_', 'REFERENCES tbl_', 'INSERT INTO tbl_', '_TS_'),
                            array(
                                'CREATE TABLE '.$_SESSION['CiiInstaller']['table_prefix'], 
                                'REFERENCES '.$_SESSION['CiiInstaller']['table_prefix'],
                                'INSERT INTO '.$_SESSION['CiiInstaller']['table_prefix'],
                                time(),
                            ),
                            $statement
                        );
                        $result = $dbh->exec($statement);
                    }
                    if($result==false)
                        $result=0;
                    $sqls[] = str_replace("\n", " ", trim($statement))."\n   ===>".$result;
                }

                header('OK', false, 200);
                //echo join("\n", $sqls);

            } catch (PDOException $e) {
                header('ERROR', false, 406);
                echo $e->getMessage();
            }
		} // }}}
		else if (isset($_POST['userCheck'])) // {{{
		{
			if (
				$_POST['userCheck']['admin_user'] == '' || 
				$_POST['userCheck']['admin_email'] == '' || 
				$_POST['userCheck']['password'] == '' || 
				$_POST['userCheck']['name'] == '' ||
				$_POST['userCheck']['siteName'] == ''
			   ) 
			{
                header('ERROR', false, 406);
                echo 'Missing Infos';
			}
			else
			{
				// Generate encryption key
				$_SESSION['CiiInstaller']['key'] = generateKey();
				
				try {
                    $sqls = array();
                    $dbh = new PDO('mysql:host='.$_SESSION['CiiInstaller']['host'].';dbname='.$_SESSION['CiiInstaller']['db'], $_SESSION['CiiInstaller']['db_user'], $_SESSION['CiiInstaller']['db_pass']);
                    $salt = uniqid('',true);                
                    $password = md5($salt.$_POST['userCheck']['password']);
                    $sql = "INSERT INTO {$_SESSION['CiiInstaller']['table_prefix']}user ("
                        ."id, username, password, salt, email, profile) VALUES ("
                        ."1,  '%s',     '%s',     '%s', '%s',  '%s');";
                    $sql = sprintf(
                        $sql, 
                        $_POST['userCheck']['admin_user'], 
                        $password, 
                        $salt, 
                        $_POST['userCheck']['admin_email'], 
                        ''
                    );
                    $result = $dbh->exec($sql);
                    $sqls[] = str_replace("\n", " ", trim($sql))."\n   ===>".$result;
                    $userId = $dbh->lastInsertId();
                    // Insert sample posts
                    $sqlDump = @fread(@fopen($DB_DATA, 'r'), @filesize($DB_DATA)) or die(header('ERROR',false,406));
                    $sqlDump = remove_remarks($sqlDump);
                    // DEBUG par($sqlDump);
                    $statements = split_sql_file($sqlDump, ';');
                    // DEBUG par($statements);
                    // Do all statements
                    foreach($statements as $n=>$statement) {
                        if(strtoupper(substr(trim($statement),0,6))=='SELECT')  {
                            $result = $dbh->query($statement);
                        } else {
                            $statement = str_replace(
                                array('CREATE TABLE tbl_', 'REFERENCES tbl_', 'INSERT INTO tbl_', '_TS_', '_USER_ID_'),
                                array(
                                    'CREATE TABLE '.$_SESSION['CiiInstaller']['table_prefix'], 
                                    'REFERENCES '.$_SESSION['CiiInstaller']['table_prefix'],
                                    'INSERT INTO '.$_SESSION['CiiInstaller']['table_prefix'],
                                    mktime(),
                                    $userId
                                ),
                                $statement
                            );
                            $result = $dbh->exec($statement);
                        }
                        if($result==false)
                            $result=0;
                        $sqls[] = str_replace("\n", " ", trim($statement))."\n   ===>".$result;
                    }



                } catch(Exception $e) {
                    header('ERROR',false,406);
                    echo $e->getMessage();
                }

		
				// Write out config file
				$config = array(
					'basePath'=>realpath(dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'protected'),
					'name'=>$_POST['userCheck']['siteName'],
					// application components
					'components'=>array(
						// Use a MySQL database
						'db'=>array(
							'class'=>'CDbConnection', 
							'connectionString' => 'mysql:host='.$_SESSION['CiiInstaller']['host'].';dbname='.$_SESSION['CiiInstaller']['db'],
							'emulatePrepare' => true,
							'username' => $_SESSION['CiiInstaller']['db_user'],
							'password' => $_SESSION['CiiInstaller']['db_pass'],
							'charset' => 'utf8',
							'schemaCachingDuration'=>3600,
                            'enableProfiling'=>true,
                            'tablePrefix'=>$_SESSION['CiiInstaller']['table_prefix'],
						),
					),
					'params'=>array(
						'yiiPath'=>$_SESSION['CiiInstaller']['yiiPath'],
						'webmasterEmail'=>$_POST['userCheck']['admin_email'],
						'editorEmail'=>$_POST['userCheck']['admin_email'],
						'encryptionKey'=>$_SESSION['CiiInstaller']['key'],
					),
                );

				$d='<?php return ';
			    buildArray($config, 0, $d);
				//$d = str_replace("'" . dirname(__FILE__).DIRECTORY_SEPARATOR, 'dirname(__FILE__).DIRECTORY_SEPARATOR.\'', $d);
			    if($fh = fopen('../protected/config/main-local.php', 'w')) { 
    			    fwrite($fh, $d);
	    		    fclose($fh);
                    header('OK', false, 200);
                    echo join("\n", $sqls);
                } else {
                    header('ERROR', false, 406);
                    echo "Can't open '../protected/config/main-local.php' for writing";
                }
                // Remove .do_install file
                unlink('../.do_install');
			}
		} // }}}
		exit();
	}

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $APP; ?> Installer</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
        <link rel="stylesheet" media="screen" type="text/css" href="css/bootstrap.min.css" />
        <link rel="stylesheet" media="screen" type="text/css" href="css/bootstrap-responsive.min.css" />
	</head>
	<body data-spy="scroll" data-target=".navbar">
		<div class="navbar navbar-inverse navbar-fixed-top"><!-- {{{ Nav Bar -->
			<div class="navbar-inner">
                <div class="container">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="#"><?php echo $APP; ?></a>
                    <div class="nav-collapse collapse">
                        <ul class="nav">
                            <li class="active"><a href="#">Installer</a></li>
                        </ul>
                    </div><!--/.nav-collapse -->
                </div>
			</div>
		</div><!-- }}} End Nav Bar -->
		<div class="container">
			<ul class="breadcrumb">
				<li>
                <a href="#"><?php echo $APP; ?></a> <span class="divider">|</span>
			  	</li>
			  	<li class="active">Welcome!</li>
			</ul>
			<div class="alert"></div>
			<div class="row-fluid">
				<!-- {{{ Welcome -->
				<div id="welcome" class="well">
					<h2>Welcome!</h2>
                    <p>Thanks for choosing <em><?php echo $APP; ?></em>! This installer will guide you through setting up your site.<br/>
                    Before we start, please make sure you have the following information at your fingertips:</p>
					<ol>
						<li>The full path to Yii Framework</li>
						<li>The hostname, database name, username and password for a MySQL database</li>
                    </ol>
                    <p><strong>Note:</strong> The database must already exist before you continue installing.</p>
                    <p>When you're ready, press the <strong>[Start]</strong> button below. This process should take no longer than 5 minutes.</p>
					<a id="welcomeButton" class="nav btn btn-primary">Start</a>
					<div style="clear:both;"></div>
				</div><!-- }}} -->
				
				<!-- {{{ Yii Framework Location -->
				<div id="yii" class="well">
					<h2>Where is Yii?</h2>
                    <p>Please provide the system path where Yii Framework is located at.</p>
                    <ul>
                        <li>The path should point to Yii's "framework" folder</li>
                        <li>The path needs to have a trailing slash (<code>/</code> on Unix systems or <code>\</code> on Windows systems).</li>
                    </ul>
					<center>
						<input id="yiiPath" type="text" placeholder="/path/to/yii/framework/" value="C:/Frameworks/yii-git/framework/" />
						<a id="yiiCheckButton" class="btn btn-inverse btn-form">Check</a>
					</center>
					<br /><br />
					<a id="yiiButton" class="nav btn btn-primary" style="display:none;">Next</a>
					<div style="clear:both;"></div>
				</div><!-- }}} -->
				
				<!-- {{{ System Check -->
				<div id="systemCheck" class="well">
					<h2>Requirements Check</h2>
                    <p>Below are the minimum requirements for <?php echo $APP;; ?>. Anything highlighted in <span class="label label-important">red label</span> need you attention.</p>
					<div id="systems-info" style="margin-left: 20px;">
						<?php
						$errors = false;
						foreach ($requirements as $k=>$v):
							echo '<li>' . $v[0] . '&nbsp;';
							echo '<span class="label label-' . ($v[1] ? 'info' : ($v[2] ? $v[2] : 'important')) .'" ' . (!$v[1] ? 'rel="tooltip" title="' . $v[3]. '"' : ''). '>' . ($v[1] ? 'OK' : (isset($v[2]) ? $v[2] : 'Error')) .'</span>';
							echo '</li>';
							
							if (!$v[1] && !$errors)
							{
								if ($v[2] == 'warning')
									$errors = false;
								else
									$errors = true;
							}
						endforeach;						
						?>
					</div>
					<br /><br />
					<a id="checkSystemButton" class="nav btn btn-primary">Check Again</a>
					<div style="clear:both;"></div>
				</div><!-- }}} -->
				
				<!-- {{{ MySQL Installation -->
				<div id="mysql" class="well">
					<h2>Database Setup</h2>
					<p>Now we're going to setup the MySQL database.</p>
					<input id="host" type="text" placeholder="Database Host" /><br />
					<input id="db" type="text" placeholder="Database Name" /><br />
					<input id="user" type="text" placeholder="Database User" /><br />
					<input id="password" type="password" placeholder="Database Password" /><br />
					<input id="table_prefix" type="text" placeholder="Table Prefix" /><br />
					
					<br /><br />
					<a id="mysqlCheckButton" class="nav btn btn-primary">Check Connection</a>
					<a id="mysqlButton" class="nav btn btn-primary" style="display:none;">Next</a>
					<div style="clear:both;"></div>
				</div><!-- }}} -->
				
				<!-- {{{ User Information -->
				<div id="user-info" class="well">
					<h2>Setup Admin User</h2>
					<input id="admin_email" type="text" placeholder="Website Admin Email" /><br />
					<input id="admin_user" type="text" placeholder="Admin Username" /><br />
					<input id="upassword" type="password" placeholder="Your Password" /><br />
					<input id="displayName" type="text" placeholder="Your Display Name" /><br />
                    <input id="siteName" type="text" placeholder="Site Name" /><br />
					<br /><br />
					<a id="userButton" class="nav btn btn-primary">Create User and Config File</a>
					<div style="clear:both;"></div>
				</div><!-- }}} -->
				
				<!-- {{{ Done -->
				<div id="done" class="well">
					<h2>All Done!</h2>
                    <p>To finish the installation, remove  the directory <em>/install</em> and click the <strong>[Launch]</strong> button.</p>
                    <p>You can change some of the blog configuration done here in the file <em>protected/config/main-local.php</em>.</p>
					<br /><br />
					<a id="launchButton" class="nav btn btn-primary">Launch</a>
					<div style="clear:both;"></div>
                </div><!-- }}} -->

			</div>
        </div>
        <style>
            <!--
            /* {{{ Styles */
            body {
                padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
                position: relative;
            }
			.container {
				width: 800px;
				margin-left: auto;
				margin-right: auto;
			}
            /*
            .navbar-inner {
				margin-top: -23px;
            }
            */
			.nav.btn-primary {
				float:right;
			}
			.nav.btn-danger {
				float:left;
			}
			
			.btn-form {
				margin-left: 10px;
				margin-top: -9px;
			}
			#yii, #systemCheck, #mysql, #user-info, #done {
				display:none;
			}
			
			.alert {
				display:none;
            }
            /* }}} ENd Styles */
            // -->
		</style>
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/bootstrap.min.js"></script>
		<script type="text/javascript">
			// Install Script for this app
			// {{{ ---------- Buttons ----------------------------------------------------------------------------------
			var previous = '';
			$("#welcomeButton").click(function() { $("#welcome").slideUp(); $("#yii").slideDown(); $("#yiiPath").focus().select(); previous = 'welcome'; });
			
			//$("#backButton").click(function() { $(".alert").slideUp(); $(this).parent().slideUp();  $("#"+previous).slideDown(); });
			
			$("#yiiButton").click(function() { $(".alert").slideUp(); $(this).parent().slideUp(); $("#systemCheck").slideDown(); previous='yii'; });
			
			$("#systemButton").click(function() { $(".alert").slideUp(); $(this).parent().slideUp(); $("#mysql").slideDown(); previous='system'; });
			
			$("#launchButton").click(function() { location.href='..'; });
			// }}} 
            // {{{ ---------- Ajax Calls -------------------------------------------------------------------------------
            
            // {{{ Check Yii
            $("#yiiCheckButton").click(function() { 
				$.ajax({ 
					'type' : 'POST', 
					'data' : { 'yiiCheck' : { 'path' : $("#yiiPath").val() }},
					'beforeSend' : function() {
						$("#yiiCheckButton").removeClass('btn-danger').removeClass('btn-inverse').addClass('btn-warning btn-form').html('Checking...');
					},
					'success' : function() {
						$("#yiiCheckButton").removeClass('btn-warning').addClass('btn-success').html('OK!');
						$("#yiiButton").slideDown();
						$(".alert").removeClass('alert-success').removeClass('alert-warning').removeClass('alert-error').addClass('alert-success').html('<strong>Got It!</strong> OK, now that I know where Yii is we can proceed. Click the <strong>[Next]</strong> button to proceed.').slideDown();
					},
					'error' : function() {
						$("#yiiCheckButton").removeClass('btn-warning').addClass('btn-danger').html('Not Found!');
						$(".alert").removeClass('alert-success').removeClass('alert-warning').removeClass('alert-error').addClass('alert-error').html('<strong>Oops!</strong> Sorry, I wasn\'t able to find Yii framework in the path you specified. Please try again.').slideDown();
					}
				}); 
			}); // }}} 

            // {{{ Check System
			$("#checkSystemButton").click(function() { 
				$.ajax({ 
					'type' : 'POST', 
					'data' : { 'systemCheck' : ''},
					'beforeSend' : function() {
						$("#systems-info").slideUp().html('');
					},
					'success' : function(data) {
						$(".alert").removeClass('alert-success').removeClass('alert-warning').removeClass('alert-error').addClass('alert-success').html('<strong>OK!</strong> Everything is configured properly.').slideDown();
						$("#checkSystemButton").parent().slideUp();
                        $("#mysql").slideDown();
                        $("#host").focus().select(); 
					},
					'error' : function() {
						$(".alert").removeClass('alert-success').removeClass('alert-warning').removeClass('alert-error').addClass('alert-error').html('Please correct the following issues before proceeding').slideDown();
					},
					'completed' : function(data) {
						$("#systems-info").html(data).slideDown();
					}
				}); 
			}); // }}} 

            // {{{ Check MySQL
			$("#mysqlCheckButton").click(function() { 
				$.ajax({ 
					'type' : 'POST', 
                        'data' : { 
                            'mysqlCheck' : { 
                                'host' : $("#host").val(), 
                                'db' : $("#db").val(), 
                                'user' : $("#user").val(), 
                                'password' : $("#password").val(),
                                'table_prefix' : $("#table_prefix").val() 
                            }
                        },
					'beforeSend' : function() {
						$(".alert").slideUp();
					},
					'success' : function(data) {
						$(".alert").removeClass('alert-success').removeClass('alert-warning').removeClass('alert-error').addClass('alert-success').html('<strong>Connected!</strong> I was able to connect to MySQL and load the database.').slideDown();
                        $("#mysqlCheckButton").parent().slideUp();
                        $("#user-info").slideDown(); 
                        previous='mysql';
                        $("#admin_email").focus().select(); 
					},
					'error' : function() {
						$(".alert").removeClass('alert-success').removeClass('alert-warning').removeClass('alert-error').addClass('alert-error').html('<strong>Hmmm...</strong> Sorry, I wasn\'t able to connect to MySQL using the credentials you provided.').slideDown();
					},
				}); 
            }); // }}} 

			// {{{ Create User
			$("#userButton").click(function() {
				$.ajax({ 
					'type' : 'POST', 
                        'data' : { 
                            'userCheck' : { 
                                'admin_email' : $("#admin_email").val(), 
                                'admin_user' :  $("#admin_user").val(), 
                                'password' :    $("#upassword").val(), 
                                'name' :        $("#displayName").val(), 
                                'siteName' :    $("#siteName").val() 
                            }
                        },
					'beforeSend' : function() {
						$(".alert").slideUp();
					},
					'success' : function(data) {
						$(".alert").removeClass('alert-success').removeClass('alert-warning').removeClass('alert-error').addClass('alert-success').html('<strong>All Good!</strong> You\'re all set!').slideDown();
						$("#userButton").parent().slideUp(); $("#done").slideDown();
					},
					'error' : function() {
						$(".alert").removeClass('alert-success').removeClass('alert-warning').removeClass('alert-error').addClass('alert-error').html('<strong>Hmmm...</strong> Looks like you forgot something... Make sure all the fields are filled out then try again.').slideDown();
					},
				}); 
            }); // }}} 

            // }}} End Ajax Calls
		</script>
	</body>
</html>
<?php
	function encryptHash($email, $password, $_dbsalt) {
		return mb_strimwidth(hash("sha512", hash("sha512", hash("whirlpool", md5($password . md5($email)))) . hash("sha512", md5($password . md5($_dbsalt))) . $_dbsalt), 0, 120);	
	}
	
	function generateKey() {
		return mb_strimwidth(hash("sha512", hash("sha512", hash("whirlpool", md5(time() . md5(time())))) . hash("sha512", time()) . time()), 0, 120);
	}
    // {{{ buildArray
    /**
     * Builds a textual output of a PHP array
     *
     * @param mixed $array Array to build
     * @param integer $level Current recursion level
     * @param string &$d textual output
     * @return void
     */
	function buildArray($array, $level = 0, &$d)
    {
        $d.= "array(\n";
        foreach ($array as $k=>$v)
        {
            if (is_array($k))
                buildArray($k, $level+1, $d);
            else if (is_array($v))
            {
                $d.= str_pad('', ($level+1)*4, " ", STR_PAD_LEFT)."'" . $k ."' => ";
                buildArray($v, $level+1, $d);
            }
			else if (is_int($k))
				$d.=str_pad('', ($level+1)*4, " ", STR_PAD_LEFT)."'" . $v . "',\n";
			else if (is_bool($v))
				$d.= str_pad('', ($level+1)*4, " ", STR_PAD_LEFT)."'" . $k . "' => " . ($v ? 'true' : 'false') .",\n";
            else
                $d.= str_pad('', ($level+1)*4, " ", STR_PAD_LEFT)."'" . $k . "' => '" . $v ."',\n";
        }
        $d.= str_pad('', ($level)*4, " ", STR_PAD_LEFT).")";
        if ($level == 0)
            $d.= ';';
        else
            $d.= ",\n";
    } // }}} 

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 fdm=marker fdc=4: */ 
?>
