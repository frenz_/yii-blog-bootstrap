<!doctype html>
<html>
<head>
    <?php Yii::app()->controller->widget('ext.seo.widgets.SeoHead', array(
        'defaultDescription'=>Yii::app()->params['appDescription'],
        'httpEquivs'=>array('Content-Type'=>'text/html; charset=utf-8', 'Content-Language'=>'en-US'),
        'title'=>array('class'=>'ext.seo.widgets.SeoTitle', 'separator'=>' :: '),
    )); ?>
    <link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/favicon.ico">
	<?php Yii::app()->bootstrap->registerAllCss(); ?>
	<?php Yii::app()->bootstrap->registerCoreScripts(); ?>
    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/css/styles.css'); ?>
    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/css/blog.css'); ?>
    <!--[if lt IE 9]>
        <script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>

<body id="top">

<?php $this->widget('bootstrap.widgets.TbNavbar',array(
    //'type'=>'inverse',
    'brand'=>CHtml::encode(Yii::app()->name),
    'collapse'=>true,
    'items'=>array(
        array(
            'class'=>'bootstrap.widgets.TbMenu',
            'items'=>array(
                array('label'=>Yii::t('site','Home'), 'url'=>Yii::app()->createUrl('/post/index'),
                        'active'=>Yii::app()->controller->id === 'post' && Yii::app()->controller->action->id === 'index'),
                array('label'=>Yii::t('site','About'), 'url'=>array('site/page','view'=>'about')),
                array('label'=>Yii::t('site','Contact'), 'url'=>array('site/contact')),
            ),
            'htmlOptions'=>array('class'=>'pull-left'),
        ),
        array(
            'class'=>'bootstrap.widgets.TbMenu',
            'items'=>array(
                array('label'=>Yii::t('site','Login'), 'url'=>Yii::app()->createUrl('/site/login'), 'visible'=>Yii::app()->user->isGuest),
                array('label'=>Yii::t('site','Logout ({username})',array('{username}'=>Yii::app()->user->name)), 'url'=>Yii::app()->createUrl('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),
                array('label'=>Yii::t('site','Language'),'url'=>'#','items'=>array(
                    array('label'=>Yii::app()->locale->getLanguage('en'),'url'=>array('/site/selectLanguage','lc'=>'en')),
                    array('label'=>Yii::app()->locale->getLanguage('de'),'url'=>array('/site/selectLanguage','lc'=>'de')),
                )),
            ),
            'htmlOptions'=>array('class'=>'pull-right'),
        ),
    ),
)); ?>

<div class="container">

    <?php if (!empty($this->breadcrumbs)):?>
        <?php $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
            'links'=>$this->breadcrumbs,
        )); ?>
    <?php endif?>

    <?php echo $content; ?>

    <hr />

    <footer>

        <p class="powered">
            Powered by <?php echo CHtml::link('Yii PHP framework', 'http://www.yiiframework.com', array('target'=>'_blank')); ?> /
            <?php echo CHtml::link('jQuery', 'http://www.jquery.com', array('target'=>'_blank')); ?> /
            <?php echo CHtml::link('Yii-Bootstrap', 'http://www.yiiframework.com/extension/bootstrap', array('target'=>'_blank')); ?> /
            <?php echo CHtml::link('Yii-LESS', 'http://www.yiiframework.com/extension/less', array('target'=>'_blank')); ?>  /
            <?php echo CHtml::link('Yii-SEO', 'http://www.yiiframework.com/extension/seo', array('target'=>'_blank')); ?> /
            <?php //echo CHtml::link('Yii-Facebook', '#', array('rel'=>'tooltip', 'title'=>'Link available soon')); ?> /
            <?php echo CHtml::link('Bootstrap', 'http://twitter.github.com/bootstrap', array('target'=>'_blank')); ?> /
            <?php echo CHtml::link('LESS', 'http://www.lesscss.org', array('target'=>'_blank')); ?>
        </p>

        <p class="copy">
            <?php echo Yii::app()->params['copyrightInfo']; ?>&nbsp;|&nbsp;
            See <a href="https://bitbucket.org/jwerner/yii-blog-bootstrap">yii-blog-bootstrap on Bitbucket</a>
        </p>

    </footer>

</div>

<?php // Yii::app()->clientScript->registerScriptFile('http://s7.addthis.com/js/250/addthis_widget.js#pubid=REPLACE_ME', CClientScript::POS_END); ?>

</body>
</html>
